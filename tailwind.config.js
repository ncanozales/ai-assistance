/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      screens: {
        bp1: "830px",
        bp2: "450px",
      },
      colors: {
        dark: "#212121",
        dark2: "#2f2f2f",
        prm: "#236CA1",
        sec: "#48d0df",
      },
      backgroundImage: {
        robot: "url('@/assets/images/wallpaper2.jpg')",
      },
      fontFamily: {
        chakra: ["Chakra Petch", "sans-serif"],
      },
    },
  },
  plugins: [],
};
