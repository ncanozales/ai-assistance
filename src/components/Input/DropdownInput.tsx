import { useState, useEffect } from "react";
import { FaAngleDown } from "react-icons/fa6";
import { OutsideClick } from "@/components/OutsideClick";
import { DropdownData } from "@/data/data";

interface DropdownInputProps {
  type: string;
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
}

const DropdownInput: React.FC<DropdownInputProps> = ({ type, value, setValue }) => {
  const data = DropdownData[type as "type" | "language"];
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (value === "") setValue(data[0] ?? "");
  }, []);

  return (
    <OutsideClick onClickOutside={() => setOpen(false)}>
      <div
        className="relative w-full border border-gray-300 text-[14px] p-2 min-h-[39px] bg-white text-prm rounded-lg cursor-pointer"
        onClick={() => setOpen(!open)}
      >
        <h2 className="">{value}</h2>
        <FaAngleDown
          className={`absolute right-[.7rem] top-1/2 -translate-y-1/2 duration-300 ${open ? "rotate-90" : ""}`}
        />

        {open ? (
          <div className="w-full absolute z-10 left-0 top-[3rem] border border-gray-300 bg-white rounded-lg overflow-hidden">
            {data.map((data, index) => (
              <h2
                key={index}
                onClick={() => {
                  setValue(data);
                  setOpen(false);
                }}
                className="p-2 hover:text-white hover:bg-prm"
              >
                {data}
              </h2>
            ))}
          </div>
        ) : (
          ""
        )}
      </div>
    </OutsideClick>
  );
};

export default DropdownInput;
