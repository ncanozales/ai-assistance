import React from "react";

interface SliderInputProps {
  value: number;
  setValue: React.Dispatch<React.SetStateAction<number>>;
  min: number;
  max: number;
}

const SliderInput: React.FC<SliderInputProps> = ({ value, setValue, min, max }) => (
  <div className="flex items-center">
    <div className="w-full flex-between">
      <input
        id="minmax-range"
        type="range"
        min={min}
        max={max}
        onChange={(e) => setValue(parseInt(e.target.value))}
        value={value}
        className="w-full h-[8px] bg-gray-200 rounded-lg appearance-none cursor-pointer   
          [&::-webkit-slider-thumb]:w-[18px]
          [&::-webkit-slider-thumb]:h-[18px]
          [&::-webkit-slider-thumb]:appearance-none
          [&::-webkit-slider-thumb]:bg-prm
          [&::-webkit-slider-thumb]:rounded-full
          [&::-webkit-slider-thumb]:transition-all
          [&::-webkit-slider-thumb]:duration-150
          [&::-webkit-slider-thumb]:ease-in-out
          "
      />

      <h1 className=" text-prm font-semibold w-[55px] text-right">
        {value}/{max}
      </h1>
    </div>
  </div>
);

export default SliderInput;
