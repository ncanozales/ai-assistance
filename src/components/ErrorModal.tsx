import React from "react";
import face2 from "@/assets/images/face2.png";

const ErrorModal: React.FC<ErrorModalProps> = ({ err, setErr }) => {
  if (err.msg === "") return <div className="animation-exit" />;

  return (
    <div className="fixed h-screen top-0 w-full duration-300 bg-black bg-opacity-60 backdrop-blur-[10px] animation-enter flex-center">
      <div className="bg-white rounded-[12px] col w-[320px] p-5 m-3">
        <img className="w-[120px] mx-auto" src={face2} alt="" />

        <h1 className="text-prm text-[22px] font-bold mt-5">Problem Detected</h1>
        <h2 className="mt-1 text-gray-500 font-semibold text-[16px]">{err.msg} </h2>

        <button
          onClick={() => {
            if (err.command) err.command();
            setErr({ msg: "", command: () => {} });
          }}
          className="mt-10 hover:scale-105 duration-300 bg-prm text-white px-2 py-3 rounded-lg font-semibold"
        >
          Confirm
        </button>
      </div>
    </div>
  );
};

export default ErrorModal;
