import { FaArrowLeft } from "react-icons/fa6";

const Prev: React.FC<{ command: () => void }> = ({ command }) => {
  return (
    <button onClick={command} className="bg-prm text-white px-4 py-2 rounded-lg flex-center">
      <FaArrowLeft />
      <span className="ml-3">Previous</span>
    </button>
  );
};

export default Prev;
