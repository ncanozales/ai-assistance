import { FaArrowRight } from "react-icons/fa6";

const Next: React.FC<{ command: () => void; title: string | undefined; className: string }> = ({
  command,
  title = "Next",
  className = "",
}) => {
  return (
    <button
      onClick={command}
      className={`${className} bg-prm duration-300 text-white px-4 py-2 rounded-lg flex-center opacity-1`}
    >
      <span className="mr-3">{title}</span>
      <FaArrowRight />
    </button>
  );
};

export default Next;
