import face from "@/assets/images/face2.png";
import { BeatLoader } from "react-spinners";

interface ChatProp {
  msg: string;
}

export const ChatLeft: React.FC<ChatProp> = ({ msg }) => (
  <div className=" w-full p-2 flex items-start">
    <img className="w-[45px] h-[45px] bg-dark2 rounded-full" src={face} alt="" />
    {msg !== "" ? (
      <p className="text-white ml-4  flex-1 whitespace-pre-line">{msg}</p>
    ) : (
      <BeatLoader
        color={"#48D0DF"}
        loading={true}
        cssOverride={{ marginTop: 15, marginLeft: 15 }}
        size={10}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
    )}
  </div>
);

export const ChatRight: React.FC<ChatProp> = ({ msg }) => (
  <div className="w-full p-2 flex-end my-2">
    <p className="w-full max-w-[500px] bg-dark2 text-white rounded-xl py-3 px-5 whitespace-pre-line">{msg}</p>
  </div>
);
