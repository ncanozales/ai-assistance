import { useState } from "react";
import { IoMdClose } from "react-icons/io";

interface Step4Props {
  instructions: string[];
  setInstructions: React.Dispatch<React.SetStateAction<string[]>>;
  step: number;
}

const Step4: React.FC<Step4Props> = ({ instructions, setInstructions, step }) => {
  const [instruction, setInstruction] = useState<string>("");

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter" && instruction !== "") {
      setInstructions((prev) => [...prev, instruction]);
      setInstruction("");
    }
  };

  const removeInstructionsByIndex = (index: number) => setInstructions((prev) => prev.filter((_, i) => i !== index));

  if (step !== 4) return null;
  return (
    <>
      <h1 className="text-prm text-[17px] font-bold">Additional Instruction (Optional)</h1>
      <p className="text-prm text-[13px] mb-3">Add some instruction on how your question should be generated</p>

      <div className="flex items-center">
        <input
          type="text"
          className="text-gray-500 border-[1px] text-[14px] border-gray-300 outline-none py-2 px-2 rounded-md w-full"
          placeholder="example: avoid questions about the location of the event"
          value={instruction}
          onChange={(e) => setInstruction(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </div>

      {instructions.length !== 0 && (
        <div className="bg-white rounded-[7px] border-[1px] border-gray-400 mt-4">
          {instructions.map((e, index) => (
            <div
              key={index}
              className="py-3 px-4 border-b last:border-b-0 border-gray-300 pr-[15px] text-[15px] text-gray-600 flex relative"
            >
              <h1 className="font-semibold mr-2">{index + 1}. </h1>
              <h1>{e}</h1>
              <IoMdClose
                onClick={() => removeInstructionsByIndex(index)}
                className="absolute right-[10px] top-[11px] text-[24px] cursor-pointer"
              />
            </div>
          ))}
        </div>
      )}
    </>
  );
};

export default Step4;
