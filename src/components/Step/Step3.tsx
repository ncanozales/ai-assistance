import React from "react";
import DropdownInput from "@/components/Input/DropdownInput";
import SliderInput from "@/components/Input/SliderInput";

interface BaseProps {
  title: string;
  subtitle: string;
  children: React.ReactNode;
}

interface Step3Props {
  difficulty: number;
  setDifficulty: React.Dispatch<React.SetStateAction<number>>;
  questionNumber: number;
  setQuestionNumber: React.Dispatch<React.SetStateAction<number>>;

  questionType: string;
  setQuestionType: React.Dispatch<React.SetStateAction<string>>;
  language: string;
  setLanguage: React.Dispatch<React.SetStateAction<string>>;
  step: number;
}

const Base: React.FC<BaseProps> = ({ title, subtitle, children }) => (
  <>
    <h1 className="text-prm text-[17px] font-bold">{title}</h1>
    <p className="text-prm text-[13px] mb-3">{subtitle}</p>

    {children}
  </>
);

const Step3: React.FC<Step3Props> = ({
  difficulty,
  setDifficulty,
  questionNumber,
  setQuestionNumber,
  questionType,
  setQuestionType,
  language,
  setLanguage,
  step,
}) => {
  if (step !== 3) return null;
  return (
    <>
      <Base title={"Question Difficulty"} subtitle={"Set the difficulty of your questions"}>
        <SliderInput {...{ value: difficulty, setValue: setDifficulty, min: 1, max: 10 }} />
      </Base>

      <div className="bg-gray-400 w-full h-[1px] my-5" />

      <Base title={"Question Number"} subtitle={"Set the number of the questions to be generated"}>
        <SliderInput {...{ value: questionNumber, setValue: setQuestionNumber, min: 1, max: 50 }} />
      </Base>

      <div className="bg-gray-400 w-full h-[1px] my-5" />

      <Base title={"Question Type"} subtitle={"Set the question type to be generated"}>
        <DropdownInput {...{ type: "type", value: questionType, setValue: setQuestionType }} />
      </Base>

      <div className="bg-gray-400 w-full h-[1px] my-5" />

      <Base title={"Language"} subtitle={"Set the language of the questions to be generated"}>
        <DropdownInput {...{ type: "language", value: language, setValue: setLanguage }} />
      </Base>
    </>
  );
};

export default Step3;
