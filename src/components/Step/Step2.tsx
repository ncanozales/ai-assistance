import FileList from "../List/FileList";

import { useRef } from "react";
import { FiUpload } from "react-icons/fi";
import { uploadPdfToServer } from "@/services/api";

interface Step2Props {
  files: File[];
  setFiles: React.Dispatch<React.SetStateAction<File[]>>;
  maxFiles: number;
  progress: number;
  setProgress: React.Dispatch<React.SetStateAction<number>>;
  setSourceId: React.Dispatch<React.SetStateAction<string>>;
  chatPDF: boolean;
  step: number;
  setErr: React.Dispatch<React.SetStateAction<errObj>>;
}

const Step2: React.FC<Step2Props> = ({
  files,
  setFiles,
  maxFiles,
  progress,
  setProgress,
  setSourceId,
  chatPDF,
  step,
  setErr,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const chooseFile = () => files.length < maxFiles && inputRef.current && inputRef.current.click();
  const removeFile = (fileName: string) => setFiles(files.filter((file) => file.name !== fileName));

  const getFileSize = (sizeInBytes: number) => {
    const sizeInKB = sizeInBytes / 1024;
    if (sizeInKB > 999) {
      const sizeInMB = sizeInKB / 1024;
      return `${sizeInMB.toFixed(0)} MB`;
    } else {
      return `${sizeInKB.toFixed(0)} KB`;
    }
  };

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFile = e.target.files?.[0];

    if (
      selectedFile &&
      (selectedFile.type === "application/pdf" ||
        selectedFile.type === "application/msword" ||
        selectedFile.type === "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
    ) {
      setFiles((prevFiles) => [...prevFiles, selectedFile]);

      if (chatPDF) {
        setProgress(0);
        try {
          const sourceId = await uploadPdfToServer(selectedFile, setProgress);
          setSourceId(sourceId);
        } catch (err) {
          setErr({
            msg: "Something wrong. Make sure the File format is supported.",
            command: () => setFiles([]),
          });
        } finally {
          setProgress(-1);
        }
      }
    } else {
      console.error("File Format is not Supported.");
    }
  };

  if (step !== 2) return null;

  return (
    <>
      <h1 className="text-prm text-[17px] font-bold mb-5">Upload Files</h1>

      <input ref={inputRef} className="hidden" type="file" accept=".pdf,.doc,.docx" onChange={handleFileChange} />

      <div
        onClick={chooseFile}
        className="bg-gray-200 outline-dashed outline-[2px] outline-prm w-full py-10 bp2:py-14 bp1:py-20 rounded-lg col-center cursor-pointer"
      >
        <FiUpload className="text-[26px] mb-2 text-prm" />
        <h1 className="text-prm text-center text-[14px] font-medium">
          {files.length === maxFiles
            ? `You Have Reached the Maximum Files: ${maxFiles}`
            : "Drop File Here or Choose File"}
        </h1>
      </div>

      <div className="flex-col-reverse bp2:flex-row flex justify-between items-center mt-3 mb-10 px-[2px]">
        <h1 className="text-[13px] text-prm font-medium">Supported Formats: PDF, Word</h1>
        <h1 className="text-[13px] text-prm font-medium">Max Size: 25 MB</h1>
      </div>

      {files.length !== 0 &&
        files.map((e, index) => {
          return <FileList key={index} {...{ name: e.name, size: getFileSize(e.size), removeFile, progress }} />;
        })}
    </>
  );
};

export default Step2;
