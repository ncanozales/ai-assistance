import React, { useState, useEffect, useRef } from "react";
import { FaArrowUp, FaStop } from "react-icons/fa6";
import { ChatLeft, ChatRight } from "../Chat";
import { chatWithPDF, generatePrompt, stopStreaming } from "@/services/api";

import useScrollUp from "@/hooks/useScrollUp";

interface Step5Props {
  step: number;
  initialPrompt: string;
  sourceId: string;
  chatPDF: boolean;
  setErr: React.Dispatch<React.SetStateAction<errObj>>;
}

interface Message {
  type: string;
  msg: string;
}

interface ChatWithPDFResult {
  fullText: string;
}

interface GeneratePromptResult {
  fullText: string;
  context: number[];
}

const Step5: React.FC<Step5Props> = ({ step, initialPrompt, sourceId, chatPDF, setErr }) => {
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const isInitialized = useRef<boolean>(false);
  const endOfMessagesRef = useRef<HTMLDivElement>(null);

  const { scrolledDown, setScrolledDown } = useScrollUp(250);

  const [chat, setChat] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [messages, setMessages] = useState<Message[]>([]);
  const [context, setContext] = useState<number[]>([]);
  const [isPrompting, setIsPrompting] = useState<boolean>(false);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter") {
      if (!e.shiftKey) {
        // Prevent default behavior (new line)
        e.preventDefault();
        handleSubmitChat();
      }
    }
  };

  const handleStopStreaming = async () => {
    stopStreaming();
    if (message !== "") setMessages((prev) => [...prev, { type: "left", msg: message }]);
    setIsPrompting(false);
    setMessage("");
  };

  const handleSubmitChat = async () => {
    if (chat !== "")
      try {
        setIsPrompting(true);
        setScrolledDown(true);

        const theChat = chat;
        setChat("");
        setMessages((prev) => [...prev, { type: "right", msg: theChat }]);

        // Generate Prompt based on Model
        let result: ChatWithPDFResult | GeneratePromptResult;

        if (chatPDF) {
          result = await chatWithPDF(theChat, sourceId, setMessage);
        } else {
          result = await generatePrompt("llama3", theChat, setMessage, context);
        }

        if (result) {
          const fullText = result.fullText;

          if ("context" in result) {
            const newContext = result.context || [];
            if (newContext.length !== 0) setContext(newContext);
          }

          setMessage("");
          setMessages((prev) => [...prev, { type: "left", msg: fullText }]);
        }
      } catch {
        setErr({ msg: "Something wrong with the Model." });
      } finally {
        setIsPrompting(false);
      }
  };

  const handlePromptInitialization = async (question: string, setErr: React.Dispatch<React.SetStateAction<errObj>>) => {
    try {
      setIsPrompting(true);

      // Initiate Prompt based on Model
      let result: ChatWithPDFResult | GeneratePromptResult;

      if (chatPDF) {
        result = await chatWithPDF(question, sourceId, setMessage);
      } else {
        result = await generatePrompt("llama3", question, setMessage, context);
      }

      if (result) {
        const fullText = result.fullText;

        if ("context" in result) {
          const newContext = result.context || [];
          if (newContext.length !== 0) setContext(newContext);
        }

        setMessage("");
        setMessages((prev) => [...prev, { type: "left", msg: fullText }]);
      }
    } catch {
      setErr({
        msg: "Something wrong with the Model. Make sure it does exist.",
        command: () => window.location.reload(),
      });
    } finally {
      setIsPrompting(false);
    }
  };

  useEffect(() => {
    if (scrolledDown) endOfMessagesRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages, message]);

  useEffect(() => {
    if (!isInitialized.current) {
      handlePromptInitialization(initialPrompt, setErr);
      isInitialized.current = true;
    }
  }, [initialPrompt]);

  useEffect(() => {
    // Modifying Textarea Behaviour

    if (textareaRef.current) {
      textareaRef.current.style.height = "auto";
      const scrollHeight = textareaRef.current.scrollHeight;
      const maxHeight = parseInt(window.getComputedStyle(textareaRef.current).lineHeight) * 7;

      if (scrollHeight > maxHeight) {
        textareaRef.current.style.height = `${maxHeight}px`;
        textareaRef.current.style.overflowY = "auto";
      } else {
        textareaRef.current.style.height = `${scrollHeight}px`;
        textareaRef.current.style.overflowY = "hidden";
      }
    }
  }, [chat]);

  if (step !== 5) return null;
  return (
    <>
      {step === 5 && (
        <div className="w-full max-w-[800px] h-full relative pb-[70px] flex flex-col justify-start items-start overflow-y-auto">
          {messages.length !== 0 &&
            messages.map((e, index) =>
              e.type === "left" ? <ChatLeft key={index} msg={e.msg} /> : <ChatRight key={index} msg={e.msg} />
            )}

          {isPrompting && <ChatLeft msg={message} />}
          <div ref={endOfMessagesRef} />

          {/* ===== Chat Input ===== */}
          <div className="bg-dark pb-4 w-full max-w-[840px] left-1/2 -translate-x-1/2 fixed bottom-0">
            <div className="bg-dark2 flex-between items-start rounded-lg py-2 px-3 mx-[20px]">
              <textarea
                ref={textareaRef}
                className="w-full mr-4 ml-2 bg-transparent h-full text-white outline-none resize-none"
                placeholder="Send Message to Me"
                rows={1}
                value={chat}
                onChange={(e) => setChat(e.target.value)}
                style={{ height: "auto", maxHeight: "calc(1.5em * 7)", lineHeight: "1.8em" }}
                onKeyDown={handleKeyDown}
                disabled={isPrompting}
              />

              <div
                onClick={!isPrompting ? handleSubmitChat : handleStopStreaming}
                className="bg-sec h-[35px] w-[35px] rounded-md flex-center"
              >
                {isPrompting ? (
                  <FaStop className="text-white text-[20px] cursor-pointer" />
                ) : (
                  <FaArrowUp className="text-white text-[20px] cursor-pointer " />
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Step5;
