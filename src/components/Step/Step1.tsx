import { modelList } from "@/data/data";
import ModelList from "../List/ModelList";

interface Step1Props {
  model: string;
  setModel: React.Dispatch<React.SetStateAction<string>>;
  step: number;
  setFiles: React.Dispatch<React.SetStateAction<File[]>>;
}

const Step1: React.FC<Step1Props> = ({ model, setModel, step, setFiles }) => {
  if (step !== 1) return null;

  const changeModel = (model: string) => {
    setModel(model);
    setFiles([]);
  };

  return (
    <div>
      <h1 className="text-prm text-[17px] font-bold mb-5">Available Model</h1>

      {modelList.map((e) => (
        <ModelList
          key={e.h1}
          {...{ title: e.h1, subTitle: e.h2, desc: e.h3, model, changeModel, name: e.name, status: e.status }}
        />
      ))}
    </div>
  );
};

export default Step1;
