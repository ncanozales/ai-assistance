import React, { useEffect, useRef } from "react";

interface OutsideClickProps {
  children: React.ReactNode;
  onClickOutside: () => void;
}

export const OutsideClick: React.FC<OutsideClickProps> = ({ children, onClickOutside }) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (ref.current && !ref.current.contains(event.target as Node)) {
        onClickOutside && onClickOutside();
      }
    };

    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [onClickOutside]);

  return <div ref={ref}>{children}</div>;
};
