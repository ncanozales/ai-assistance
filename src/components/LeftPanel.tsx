import { leftPanelData } from "@/data/data";
import image from "@/assets/images/image1.png";

interface SideInfoProps {
  no: number;
  title: string;
  subTitle: string;
  step: number;
}

const SideInfo: React.FC<SideInfoProps> = ({ no, title, subTitle, step }) => {
  const step4 = step + no === 7;

  return (
    <div className={`${no !== step && !step4 ? "hidden bp1:flex" : ""} mb-0 bp1:mb-5 flex items-center`}>
      <p
        className={`${
          step === no || step4 ? "bg-white text-sec" : "text-white"
        } w-[40px] h-[40px] border-white border-[1px] rounded-full mr-3 flex-center font-bold duration-300`}
      >
        {no}
      </p>
      <div>
        <p className="text-white">{title}</p>
        <p className="text-sec font-semibold">{subTitle}</p>
      </div>
    </div>
  );
};

const LeftPanel: React.FC<{ step: number }> = ({ step }) => {
  return (
    <div className="w-full bp1:w-[300px] h-full flex justify-center items-center font-chakra mb-7 bp1:mb-0">
      <div className="bg-prm h-full bp1:h-[600px] m-0 bp1:m-2 p-3 rounded-lg w-full relative overflow-hidden">
        {step !== 5 &&
          leftPanelData.map((e) => <SideInfo key={e.no} {...{ no: e.no, title: e.h1, subTitle: e.h2, step }} />)}
        <img className="hidden bp1:block absolute bottom-0 left-0 w-full h-[360px] duration-300" src={image} alt="" />
      </div>
    </div>
  );
};

export default LeftPanel;
