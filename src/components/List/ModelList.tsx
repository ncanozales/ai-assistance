interface ModelListProps {
  title: string;
  subTitle: string;
  desc: string;
  model: string;
  changeModel: (e: string) => void;
  name: string;
  status: string;
}

const ModelList: React.FC<ModelListProps> = ({ title, subTitle, desc, model, changeModel, name, status }) => {
  const active = name === model;
  const unavailable = status === "unavailable";

  return (
    <div
      onClick={() => changeModel(name)}
      className={`${
        active ? "border-prm" : "border-gray-400"
      } col border-[1px] rounded-lg p-3 cursor-pointer mb-3 font-chakra`}
    >
      <div className="flex items-center mb-1">
        <div
          className={`${
            active ? "border-prm border-[7px]" : "border-gray-400 border-[3px]"
          } bg-white w-[22px] h-[22px] rounded-full`}
        />

        <div className="flex-between w-full">
          <h1 className={`${active ? "text-prm ml-[11.7px]" : "text-gray-500 ml-[12px]"} font-semibold mb-[2px] `}>
            {title} {active && unavailable ? "- Unavailable" : null}
          </h1>
        </div>
      </div>

      <h1 className={`${active ? "text-prm" : "text-gray-500"} text-[14px] font-normal ml-[34px]`}>{subTitle}</h1>
      <h1 className={`${active ? "text-prm" : "text-gray-500"} text-[14px] font-normal ml-[34px]`}>{desc}</h1>
    </div>
  );
};

export default ModelList;
