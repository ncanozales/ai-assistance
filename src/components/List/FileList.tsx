import { IoMdClose } from "react-icons/io";
import { DOCSvg, PDFSvg } from "@/assets/images/svgs";

interface FileListProps {
  name: string;
  size: string;
  removeFile: (name: string) => void;
  progress: number;
}
const FileList: React.FC<FileListProps> = ({ name, size, removeFile, progress }) => {
  const splitedFileName = name.split(".");
  const fileType = splitedFileName[splitedFileName.length - 1];

  return (
    <div className="bg-gray-200 border-[1px] border-gray-300 w-full px-4 py-3 rounded-md mb-3 flex flex-col duration-300">
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          <p className="">{fileType === "pdf" ? <PDFSvg /> : <DOCSvg />}</p>

          <div className="ml-3">
            <p className="text-prm text-[14px] font-medium break-all">{name}</p>
            <p className="text-prm text-[14px] font-medium">{size}</p>
          </div>
        </div>
        {progress === -1 && (
          <IoMdClose onClick={() => removeFile(name)} className="text-prm text-[24px] cursor-pointer min-w-[30px]" />
        )}
      </div>

      {progress !== -1 && (
        <div className="flex-between mt-2">
          <div className="bg-white border-[2px] border-white w-full h-[8px] rounded-full">
            <div className={`bg-prm h-full rounded-full duration-300`} style={{ width: `${progress}%` }} />
          </div>

          <p className="ml-3 font-medium text-prm text-[14px]">{progress}%</p>
        </div>
      )}
    </div>
  );
};

export default FileList;
