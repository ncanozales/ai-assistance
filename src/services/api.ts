import React from "react";
import axios from "axios";

const url = import.meta.env.VITE_LOCAL_BACKEND_URL || "";
const apiKey = import.meta.env.VITE_SERVER_API_KEY || "";
const serverUrlChat = import.meta.env.VITE_SERVER_BACKEND_URL || "";
const serverUrlUpload = import.meta.env.VITE_SERVER_UPLOAD_URL || "";

let abortController: AbortController | undefined;

export const stopStreaming = () => {
  if (abortController) {
    abortController.abort();
  }
};

export const uploadPdfToServer = async (file: File, setProgress: React.Dispatch<React.SetStateAction<number>>) => {
  try {
    if (!file) throw new Error("No File");

    const formData = new FormData();
    formData.append("file", file);

    const response = await axios.post(serverUrlUpload, formData, {
      headers: {
        "x-api-key": apiKey,
      },
      onUploadProgress: (progressEvent) => {
        const percentComplete = Math.round((progressEvent.loaded * 100) / progressEvent?.total);
        setProgress(percentComplete);
        console.log(percentComplete);
      },
    });

    console.log("Hasil Response : ", response.data.sourceId);

    return response.data.sourceId;
  } catch (error) {
    throw new Error(String(error));
    // return error; tak bisa pakai ini
  }
};

export const chatWithPDF = async (
  question: string,
  sourceId: string,
  setText: React.Dispatch<React.SetStateAction<string>>
) => {
  abortController = new AbortController();
  const signal = abortController.signal;

  const config = {
    headers: {
      "x-api-key": apiKey,
      "Content-Type": "application/json",
    },
  };

  const data = {
    stream: true,
    sourceId: sourceId,
    messages: [
      {
        role: "user",
        content: question,
      },
    ],
  };

  try {
    const response = await fetch(serverUrlChat, {
      method: "POST",
      headers: config.headers,
      body: JSON.stringify(data),
      signal: signal,
    });

    if (!response.ok) throw new Error("Network response was not ok");
    if (!response.body) throw new Error("Response body is null");

    const reader = response.body.getReader();
    const decoder = new TextDecoder("utf-8");

    const chunks = [];
    let receivedLength = 0;

    // eslint-disable-next-line no-constant-condition
    while (true) {
      const { done, value } = await reader.read();

      if (done) {
        console.log("End of stream");
        break;
      }

      chunks.push(value);
      receivedLength += value.length;

      // Decode
      const text = decoder.decode(value, { stream: true });
      setText((prev) => prev + text);
    }

    // Combine chunks into a single string
    const combinedChunks = new Uint8Array(receivedLength);

    let position = 0;
    for (const chunk of chunks) {
      combinedChunks.set(chunk, position);
      position += chunk.length;
    }

    const finalText = decoder.decode(combinedChunks);
    return { fullText: finalText };
  } catch (error) {
    console.error("Error:", (error as Error).message);
    throw new Error("Something Wrong.");
  }
};

export const generatePrompt = async (
  model: string,
  question: string,
  setText: React.Dispatch<React.SetStateAction<string>>,
  prevContext: number[]
) => {
  abortController = new AbortController();
  const signal = abortController.signal;

  const data = {
    model: model,
    prompt: question,
    stream: true,
    context: prevContext,
  };

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
      signal: signal,
    });

    if (!response.ok) throw new Error("Network response was not ok");
    if (!response.body) throw new Error("Response body is null");

    const reader = response.body.getReader();
    const decoder = new TextDecoder("utf-8");

    let done: boolean = false;
    let fullResponse: string = "";
    let fullText: string = "";
    let newContext: number[] = [];

    while (!done) {
      const { value, done: streamDone } = await reader.read();
      done = streamDone;

      if (value) {
        const chunk = decoder.decode(value, { stream: true });
        fullResponse += chunk;

        // Parse each line to get JSON Objects
        const lines = chunk.split("\n").filter(Boolean);

        lines.forEach((line) => {
          try {
            const parsedLine = JSON.parse(line);

            if (parsedLine.context) newContext = parsedLine.context;
            if (parsedLine.response) {
              fullText += parsedLine.response;
              setText((prev) => prev + parsedLine.response);
            }
          } catch (err) {
            console.error("Error parsing JSON:", err);
            console.log(fullResponse);
          }
        });
      }
    }

    return { fullText, context: newContext };
  } catch (error) {
    if ((error as Error).name === "AbortError") console.error("Stopped by user.");
    console.error("Fetch error:", error);
    throw new Error("Fetch error");
  }
};
