type errObj = {
  msg?: string;
  command?: () => void;
};

interface ErrorModalProps {
  err: errObj;
  setErr: React.Dispatch<React.SetStateAction<errObj>>;
}

declare module "pdf-parse" {
  const pdfParse: (data: Buffer | Uint8Array | ArrayBuffer) => Promise<{ text: string }>;
  export default pdfParse;
}
