import { useState, useEffect } from "react";

const useScrollUp = (scrollThreshold: number = 500) => {
  const [hasScrolled, setHasScrolled] = useState<boolean>(true);
  const [lastScrollPosition, setLastScrollPosition] = useState<number>(0);

  const handleScroll = () => {
    const currentScrollPosition = window.scrollY;
    const scrolledDistance = currentScrollPosition - lastScrollPosition;

    if (scrolledDistance >= scrollThreshold) {
      setHasScrolled(true);
      setLastScrollPosition(currentScrollPosition);
    } else if (scrolledDistance < 0) {
      setHasScrolled(false);
      setLastScrollPosition(currentScrollPosition);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [lastScrollPosition]);

  return { scrolledDown: hasScrolled, setScrolledDown: setHasScrolled };
};

export default useScrollUp;
