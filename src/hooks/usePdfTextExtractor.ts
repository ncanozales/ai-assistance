import { useState, useEffect } from "react";

const usePdfTextExtractor = (files: File[]) => {
  const [pdfjsLib, setPdfjsLib] = useState(null);
  const [extractedText, setExtractedText] = useState("");

  useEffect(() => {
    const loadPdfJsLib = () => {
      const script = document.createElement("script");
      script.src = "/pdfjs/pdf.min.js"; // Path to your locally stored pdf.min.js
      script.onload = () => {
        const lib = window["pdfjs-dist/build/pdf"];
        lib.GlobalWorkerOptions.workerSrc = "/pdfjs/pdf.worker.min.js"; // Path to your locally stored pdf.worker.min.js
        setPdfjsLib(lib);
      };
      document.body.appendChild(script);
    };

    loadPdfJsLib();
  }, []);

  useEffect(() => {
    if (!pdfjsLib || files.length === 0) return;

    const extractTextFromPdf = async (file: File, index: number) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      return new Promise<string>((resolve, reject) => {
        reader.onload = async () => {
          const url = reader.result;
          try {
            const pdf = await pdfjsLib.getDocument(url).promise;
            const pages = pdf.numPages;
            let fullText = `>>> BEGINNING OF DOCUMENT NUMBER ${index + 1} <<< `;

            for (let i = 1; i <= pages; i++) {
              const page = await pdf.getPage(i);
              const txt = await page.getTextContent();
              const text = txt.items.map((s) => s.str).join(" ");
              fullText += text + "\n";
            }

            fullText += `>>> END OF DOCUMENT NUMBER ${index + 1} <<<`;
            resolve(fullText);
          } catch (err) {
            resolve(`Error processing PDF number ${index + 1}`);
          }
        };
        reader.onerror = () => reject(reader.error);
      });
    };

    const extractTextFromAllPdfs = async () => {
      setExtractedText("");

      const texts = await Promise.all(files.map((file, index) => extractTextFromPdf(file, index)));
      setExtractedText(texts.join("\n"));
    };

    extractTextFromAllPdfs();
  }, [pdfjsLib, files]);

  return extractedText;
};

export default usePdfTextExtractor;
