import mammoth from "mammoth";

export const extractTextFromDOC = (file: File, index: number): Promise<string> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      if (reader.result instanceof ArrayBuffer) {
        mammoth
          .extractRawText({ arrayBuffer: reader.result })
          .then((result) => {
            const extractedText = result.value;
            const formattedText = `
            >>> BEGINNING OF DOCUMENT NUMBER ${index + 1} <<< 
            ${extractedText}
            >>> END OF DOCUMENT NUMBER ${index + 1} <<< 
          `;
            resolve(formattedText);
          })
          .catch((err) => reject("Error when Extracting Text: " + err));
      } else {
        reject("Error when Extracting Text: Result is not an ArrayBuffer.");
      }
    };

    reader.onerror = (err) => reject("Error when Extracting Text: " + err);
    reader.readAsArrayBuffer(file);
  });
};
