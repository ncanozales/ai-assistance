export const generatePromptMsg = (
  extractedDocument: string,
  questionNumber: number,
  difficulty: number,
  questionType: string,
  language: string,
  instructions: string[]
) => {
  return `
    BELOW IS THE SOURCE DOCUMENT: \n
    ${extractedDocument} \n

    NOTE THAT, EACH DOCUMENTS (SEPERATED BY BEGINNING OF DOCUMENT NUMBER, IS UNIQUE AND NOT RELATED TO EACH OTHER).
    BASED ON THOSE DOCUMENTS, GENERATE A QUESTIONS WITH BELOW SPECIFICATIONS: \n
    1. The number of the questions is : ${questionNumber} \n
    2. The difficulty of the questions is : ${difficulty}/10 \n
    3. The Question Type is : ${questionType} \n
    4. The Question Language is : ${language} language. (you generate question on ${language} language) \n
    5. If the question type is Multiple Choice, then there will be 5 choices (a, b, c, d, e). \n
    6. Provide me the Answer key to the questions you generated, unless the Question Type is Essay. \n
    7. Give newspace on every new question number. \n
    8. Answer this FORMALLY. Dont give a joke, humour, or something else. \n
    ${
      instructions.length !== 0 &&
      instructions.map((instruction, index: number) => `${index + 9}. ${instruction}`).join("\n")
    } \n

    END THE MESSAGE WITH BELOW SENTENCE: 
    Please let me know if you would like to adjust or edit any of these questions.
    `;
};
