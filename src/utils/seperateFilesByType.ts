const seperateFilesByType = (files: File[]) => {
  const pdfFiles: File[] = [];
  const docFiles: File[] = [];

  files.forEach((file) => {
    const fileType = file.type;

    if (fileType === "application/pdf") {
      pdfFiles.push(file);
    } else if (
      fileType === "application/msword" ||
      fileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ) {
      docFiles.push(file);
    }
  });

  return { pdfFiles, docFiles };
};

export default seperateFilesByType;
