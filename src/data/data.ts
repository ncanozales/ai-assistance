export const leftPanelData = [
  { no: 1, h1: "Step 1", h2: "Select Model" },
  { no: 2, h1: "Step 2", h2: "Upload File" },
  { no: 3, h1: "Step 3", h2: "Add Instruction" },
];

export const modelList = [
  {
    name: "llama3-8b",
    status: "available",
    h1: "Llama 3 (8B)",
    h2: "Free and Open Source Model. ",
    h3: "Unlimited reads and writes.",
  },
  {
    name: "phi-3-3b",
    status: "unavailable",
    h1: "Phi-3 (3B)",
    h2: "Free and Open Source Model.",
    h3: "Unlimited reads and writes.",
  },
  {
    name: "chatpdf",
    status: "available",
    h1: "GPT 3.5 Turbo",
    h2: "Commercially Licensed. ",
    h3: "Limited reads and writes.",
  },
];

export const DropdownData = {
  type: ["Essay", "Short Answer", "Multiple Choice"],
  language: ["English", "Indonesia", "Mandarin"],
};
