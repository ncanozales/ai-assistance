import { useNavigate } from "react-router-dom";

const Home = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className="max-[1400px]:hidden h-screen w-full bg-robot bg-cover bg-center bg-fixed bg-no-repeat flex-end overflow-hidden font-chakra">
        <div className="h-full w-[50%] col justify-center">
          <div onClick={() => navigate("/generate")} className="w-min hover:scale-110 duration-300 cursor-pointer">
            <h1 className="text-[40px] z-10 text-grad from-prm to-sec mr-32 w-max">Empowering Teachers with AI</h1>
            <h1 className="text-[40px] z-10 text-grad from-prm to-sec mr-32 w-max">
              Revolutionizing Question Creation
            </h1>
          </div>
        </div>
      </div>

      <div className="hidden min-[1399px]:block bg-black h-screen font-chakra flex-center">
        <h1
          onClick={() => navigate("/generate")}
          className="text-[26px] bp2:text-[32px] bp1:text-[40px] z-10 text-grad from-prm to-sec 
          text-center hover:scale-110 duration-300 cursor-pointer mx-5"
        >
          Start Generating Question with Large Language Model
        </h1>
      </div>
    </>
  );
};

export default Home;
