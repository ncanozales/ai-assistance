const NotFound = () => {
  return (
    <div className="bg-black h-screen font-chakra flex-center">
      <h1 className="text-[24px] text-grad from-prm to-sec mx-5">Page Not Found</h1>
    </div>
  );
};

export default NotFound;
