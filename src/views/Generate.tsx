import { useState, useEffect } from "react";
import { generatePromptMsg } from "@/utils/prompting";
import { extractTextFromDOC } from "@/utils/extractText";

import Step1 from "@/components/Step/Step1";
import Step2 from "@/components/Step/Step2";
import Step3 from "@/components/Step/Step3";
import Step4 from "@/components/Step/Step4";
import Step5 from "@/components/Step/Step5";

import Next from "@/components/Button/Next";
import Prev from "@/components/Button/Prev";
import LeftPanel from "@/components/LeftPanel";
import ErrorModal from "@/components/ErrorModal";
import seperateFilesByType from "@/utils/seperateFilesByType";
import usePdfTextExtractor from "@/hooks/usePdfTextExtractor";

const Generate = () => {
  const [err, setErr] = useState<errObj>({ msg: "" });
  const [step, setStep] = useState<number>(1);
  const lastStep = step === 5;

  // Step 1
  const [model, setModel] = useState<string>("");
  const chatPDF = model === "chatpdf";

  // Step 2
  const maxFiles: number = chatPDF ? 1 : 2;
  const [files, setFiles] = useState<File[]>([]);
  const [docFiles, setDocFiles] = useState<File[]>([]);
  const [pdfFiles, setPdfFiles] = useState<File[]>([]);

  const [progress, setProgress] = useState<number>(-1);
  const [sourceId, setSourceId] = useState<string>("");

  const extractedPDF = usePdfTextExtractor(pdfFiles);
  const [extractedDocument, setExtractedDocument] = useState<string>("");

  // Step 3
  const [difficulty, setDifficulty] = useState<number>(1);
  const [questionNumber, setQuestionNumber] = useState<number>(1);
  const [language, setLanguage] = useState<string>("");
  const [questionType, setQuestionType] = useState<string>("");

  // Step 4
  const [instructions, setInstructions] = useState<string[]>([]);

  useEffect(() => {
    const { pdfFiles, docFiles } = seperateFilesByType(files);
    setPdfFiles(pdfFiles);
    setDocFiles(docFiles);
  }, [files]);

  const initialPrompt = generatePromptMsg(
    `${extractedPDF} ${extractedDocument}`,
    questionNumber,
    difficulty,
    questionType,
    language,
    instructions
  );

  const handleExtractText = () => {
    if (docFiles.length !== 0) {
      const pdfIndex = pdfFiles.length;
      setExtractedDocument("");

      files.forEach(async (file, index) => {
        try {
          const newText = await extractTextFromDOC(file, index + pdfIndex);
          setExtractedDocument((prev) => `${prev} \n ${newText}`);
        } catch {
          setExtractedDocument("");
        }
      });
    }
  };

  const allowedNext = () => {
    if (step === 1) {
      return model === "llama3-8b" || model === "chatpdf";
    } else if (step === 2) {
      if (!chatPDF) {
        return files.length !== 0;
      }
      return sourceId !== "";
    } else if (step === 3) {
      return questionType !== "" && language !== "";
    }
    return true;
  };

  const goNext = () => {
    if (allowedNext()) {
      if (step === 2 && !chatPDF) handleExtractText();
      setStep(step + 1);
    }
  };

  const goPrev = () => setStep(step - 1);

  return (
    <div className="bg-grad min-h-screen flex justify-center items-start bp1:items-center">
      <div
        className={`${
          lastStep
            ? "w-full bg-dark min-h-screen p-1 bp2:p-4"
            : "w-full min-h-[650px] bp1:min-h-0 bp1:w-[900px] bg-gray-100 rounded-lg mb-12 m-3 bp2:mx-5 bp2:mt-5 p-4"
        }  duration-1000 flex flex-col bp1:flex-row `}
      >
        {!lastStep && <LeftPanel step={step} />}

        <div className={`${lastStep ? "col-start" : "col-between"} flex-1 m-0 bp1:m-2 bp1:pl-3`}>
          <div className={`${lastStep ? "w-full flex-center" : ""} h-full`}>
            <Step1 {...{ model, setModel, step, setFiles }} />
            <Step2 {...{ files, setFiles, maxFiles, progress, setProgress, setSourceId, chatPDF, step, setErr }} />

            <Step3
              {...{
                questionNumber,
                setQuestionNumber,
                difficulty,
                setDifficulty,
                questionType,
                setQuestionType,
                language,
                setLanguage,
                step,
              }}
            />

            <Step4 {...{ instructions, setInstructions, step }} />
            {lastStep && <Step5 {...{ step, initialPrompt, sourceId, chatPDF, setErr }} />}
          </div>

          {!lastStep && (
            <div className={`${step === 1 ? "flex-end" : "flex-between"} mt-7`}>
              {step !== 1 && <Prev command={goPrev} />}
              <Next
                className={allowedNext() ? "animation-enter" : "animation-exit"}
                title={step === 4 ? "Generate Answer" : undefined}
                command={goNext}
              />
            </div>
          )}
        </div>
      </div>

      <ErrorModal err={err} setErr={setErr} />
    </div>
  );
};

export default Generate;
