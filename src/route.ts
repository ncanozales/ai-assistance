import Home from "./views/Home";
import NotFound from "./views/NotFound";
import Generate from "./views/Generate";
import TesterDelsoon from "./views/TesterDelsoon";

import { createBrowserRouter } from "react-router-dom";

const router = createBrowserRouter([
  { path: "/", Component: Home },
  { path: "/tester", Component: TesterDelsoon },
  { path: "/generate", Component: Generate },
  { path: "*", Component: NotFound },
]);

export default router;
